<?php
ob_start();
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Font Awesome -->
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet" /> -->
    <!-- Google Fonts -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" /> -->
    <!-- MDB -->
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/5.0.0/mdb.min.css" rel="stylesheet" /> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet prefetch" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <title>Đăng kí sinh viên</title>
    <style>
        #main {
            width: 550px;
            border: 2px solid #4d7aa2;
            border-radius: 8px;
            padding: 32px 80px;
            display: block;

            margin: auto;
            /* box-sizing: border-box; */


            /* height: 400px; */
        }

        #main .wrapper {

            width: 100%;

            display: flex;
            align-items: center;
            flex-direction: column;
        }

        .wrapper .showError {

            width: 100%;
        }

        .showError .errorMsg {
            color: red;
        }


        form {
            width: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            margin-top: 16px;
            /* margin-bottom: 32px; */
        }



        .form-group {
            width: 100%;
            display: flex;
            margin-bottom: 16px;

        }

        .form-group .form-label {
            align-items: center;
            display: flex;
            justify-content: center;
            width: 30%;
            text-align: center;
            background-color: #70ad47;
            padding: 8px 12px;
            border: 2px solid #4d7aa2;
            border-radius: 2px;
            margin-right: 16px;
            margin-bottom: 0;
            color: #fff;


        }

        .form-group .form-label .required {
            color: red;
            margin-left: 4px;
        }


        .form-group .form-input {
            width: 70%;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: row;
            flex: 1 1 0;
            /* line-height: 32px; */

        }



        .form-group .form-input input[type="text"] {
            width: 100%;
            font-size: 20px;
            padding: 8px 4px;
            border: 2px solid #4d7aa2;
            border-radius: 2px;
        }

        #datepicker {
            width: 180px;
            margin: 0 20px 20px 20px;
        }

        #datepicker>span:hover {
            cursor: pointer;
        }

        .form-group .form-input input[type="radio"] {
            /* width: 30%; */
            /* font-size: 12px; */

            border: 1px solid #4d7aa2;
            background-color: #5b9bd5;
            width: 16px;
        }

        .form-group .form-input input[type="radio"]:hover {
            /* border: 1px solid #4d7aa2; */
            cursor: pointer;
            background-color: #5b9bd5;
        }

        .form-group .form-input .radio-label {
            margin-left: 8px;
            margin-bottom: 0;
            font-weight: 500;
        }

        .form-group .form-input #falcuties {
            width: 100%;
            font-size: 20px;
            padding: 10px 4px;
            border: 2px solid #4d7aa2;
            border-radius: 2px;

        }

        select {
            background: url("data:image/svg+xml,<svg height='20px' width='20px' viewBox='0 0 16 16' fill='%23000000' xmlns='http://www.w3.org/2000/svg'><path d='M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z'/></svg>") no-repeat;
            background-position: calc(100% - 0.75rem) center !important;
            -moz-appearance: none !important;
            -webkit-appearance: none !important;
            appearance: none !important;
            padding-right: 2rem !important;
        }

        .form-group .form-input .radio-group {
            display: flex;
            flex-direction: row;
            margin-right: 24px;
        }

        .form-group .form-input .radio-group label {
            font-size: 20px;
        }

        form .submit-btn {
            text-align: center;
            width: 120px;
            color: #fff;
            background-color: #70ad47;
            font-size: 14px;
            padding: 12px;
            border: 2px solid #4d7aa2;
            border-radius: 6px;
        }
    </style>
</head>

<body>
    <div id="main">
        <div class="wrapper">
            <div class="showError">
                <?php
                $nameErr = $falcutyErr = $genderErr = $dobErr = $addressErr = "";
                $name = $falcuty = $gender = $dob = $address = $image = "";


                if (isset($_POST['submit'])) {


                    // Validate name
                    if (empty($_POST["name"])) {
                        $nameErr = "Hãy nhập họ tên";
                        echo '<p class="errorMsg">' . $nameErr . '</p>';
                    } else {
                        if (!preg_match("^[a-zA-Z\'\-\040]+$^", removeAscent($_POST["name"]))) {
                            $nameErr = "Tên không đúng định dạng";
                            echo '<p class="errorMsg">' . $nameErr . '</p>';
                        } else {
                            $name = test_input($_POST['name']);
                        }
                    }
                    // Validate falcuty
                    if (empty($_POST["falcuty"]) || $_POST["falcuty"] == 'None') {
                        $falcutyErr = "Hãy nhập  tên khoa";
                        echo '<p class="errorMsg">' . $falcutyErr . '</p>';
                    } else {
                        $falcuty = test_input($_POST['falcuty']);
                    }

                    //Validate gender
                    if (empty($_POST["gender"])) {
                        $genderErr = "Bạn cần nhập giới tính";
                        echo '<p class="errorMsg">' . $genderErr . '</p>';
                    } else {
                        $gender = test_input($_POST['gender']);
                    }

                    // Validate date of birth
                    if (empty($_POST["dob"])) {
                        $dobErr = "Hãy nhập ngày sinh";
                        echo '<p class="errorMsg">' . $dobErr . '</p>';
                    } else {
                        $dob = test_input($_POST['dob']);
                    }

                    // Set address

                    $address = test_input($_POST['address']);



                    //upload file 

                    $allowUpload   = true;
                    $target_dir    = "upload/";
                    if (!is_dir($target_dir)) {
                        mkdir($target_dir);
                    }
                    if (!empty($_FILES['image']["name"])) {
                        date_default_timezone_set('Asia/Ho_Chi_Minh');
                        $target_file   = $target_dir . basename($_FILES["image"]["name"]);
                        $fileName = pathinfo($_FILES["image"]["name"]);
                        $savedFileName = $target_dir . $fileName["filename"] . '_' . date("YmdHis") . '.' . $fileName["extension"];


                        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
                        $allowtypes    = array('jpg', 'png', 'jpeg');
                        if (!in_array($imageFileType, $allowtypes)) {
                            echo '<p class="errorMsg">Chỉ upload ảnh định dạng png, jpg, jpeg</p>';

                            $allowUpload = false;
                        }

                        if (!$_FILES["image"]["error"]) {
                            move_uploaded_file($_FILES["image"]["tmp_name"], $savedFileName);
                        } else {
                            echo '<p class="errorMsg">' . var_dump($_FILES["image"]) . '</p>';
                            // echo $_FILES["image"]["size"];
                            $allowUpload = false;
                        }
                    } else {
                        $allowUpload = true;
                    }


                    if (empty($addressErr) && empty($dobErr) && empty($genderErr) && empty($falcutyErr) && empty($dobErr) && empty($nameErr) && $allowUpload) {
                        $_SESSION = $_POST;
                        $_SESSION["image"] = $savedFileName;

                        header("Location: confirm.php");
                    }
                }

                function test_input($data)
                {
                    $data = trim($data);
                    $data = stripslashes($data);
                    $data = htmlspecialchars($data);
                    return $data;
                }

                function removeAscent($name)
                {
                    if ($name === null) return $name;
                    $name = strtolower($name);
                    $name = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $name);
                    $name = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $name);
                    $name = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $name);
                    $name = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $name);
                    $name = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $name);
                    $name = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $name);
                    $name = preg_replace("/(đ)/", 'd', $name);
                    return $name;
                }
                ?>
            </div>
            <form method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="form-label">Họ và tên <span class="required">*</span></label>
                    <div class="form-input">
                        <input type="text" name="name" id="name" value="<?php echo $name ?>">
                    </div>

                </div>
                <div class="form-group">
                    <label class="form-label">Giới tính <span class="required">*</span></label>
                    <div class="form-input">
                        <?php
                        $genderArr = array("0" => "Nam", "1" => "Nữ");
                        for ($i = 0; $i < count($genderArr); $i++) {
                            echo '  <div class="radio-group">
                                        <input type="radio"  name="gender" value=" ' . $i . '"  >
                                        <label class="radio-label" for="' . $genderArr[$i] . '">' . $genderArr[$i] . '</label>
                                    </div>';
                        }
                        ?>
                    </div>


                </div>
                <div class="form-group">
                    <label class="form-label">Phân khoa <span class="required">*</span></label>
                    <div class="form-input">
                        <select name="falcuty" id="falcuties">
                            <?php
                            $falcuty = array("None" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học dữ liệu");
                            foreach ($falcuty as $key => $value) {
                                echo '<option value="' . $key . '">' . $value . '</option>';
                            }
                            ?>
                        </select>
                    </div>


                </div>
                <div class="form-group">
                    <label class="form-label">Ngày sinh <span class="required">*</span></label>

                    <!-- <div class="form-input">
                    </div> -->
                    <div class="form-input">
                        <input id="txtDate" type="text" name="dob" placeholder="dd/mm/yyyy" value="<?php echo $dob ?>" />

                    </div>

                </div>
                <div class="form-group">
                    <label class="form-label">Địa chỉ</label>
                    <div class="form-input">
                        <input type="text" name="address" id="address" value="<?php echo $address ?>">
                    </div>

                </div>
                <div class="form-group">
                    <label class="form-label">Hình ảnh</label>
                    <div class="form-input">
                        <input type="file" name="image" id="image" style="margin-right:-12px;">

                    </div>

                </div>
                <!-- <input class="img_encode" name="img_encode" style="display: none;" /> -->

                <input type="submit" name="submit" class="submit-btn" value="Đăng kí">
            </form>
        </div>
    </div>
    <Script>
        // function readURL(input) {
        //     console.log(input.files[0]);
        //     if (input.files && input.files[0]) {
        //         var reader = new FileReader();
        //         $('#blah').css("display", "block");
        //         reader.onload = function(e) {
        //             $('#blah')
        //                 .attr('src', e.target.result);

        //             $('.img_encode').val(e.target.result);
        //         };

        //         reader.readAsDataURL(input.files[0]);
        //     } else {
        //         $('#blah').css("display", "none");
        //     }
        // }
    </Script>
    <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
    <!-- Bootstrap -->
    <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- Bootstrap DatePicker -->
    <script type="text/javascript">
        $(function() {
            $('#txtDate').datepicker({
                format: "dd/mm/yyyy"
            });
        });
    </script>

</body>

</html>