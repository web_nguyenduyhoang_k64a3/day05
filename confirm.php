<?php
session_start();

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Font Awesome -->
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet" /> -->
    <!-- Google Fonts -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" /> -->
    <!-- MDB -->
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/5.0.0/mdb.min.css" rel="stylesheet" /> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet prefetch" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <title>Đăng kí sinh viên</title>
    <style>
        #main {
            width: 550px;
            border: 2px solid #4d7aa2;
            border-radius: 8px;
            padding: 32px 80px;
            display: block;

            margin: auto;
            /* box-sizing: border-box; */


            /* height: 400px; */
        }

        #main .wrapper {

            width: 100%;

            display: flex;
            align-items: center;
            flex-direction: column;
        }

        .wrapper .showError {

            width: 100%;
        }


        .formDisplay {
            width: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            margin-top: 16px;
            /* margin-bottom: 32px; */
        }

        .form-group {
            width: 100%;
            display: flex;
            margin-bottom: 16px;

        }

        .form-group .form-label {
            align-items: center;
            display: flex;
            justify-content: center;
            width: 30%;
            max-height: 40px;
            text-align: center;
            background-color: #70ad47;
            padding: 8px 12px;
            border: 2px solid #4d7aa2;
            border-radius: 2px;
            margin-right: 16px;
            margin-bottom: 0;
            color: #fff;


        }

        .form-group .form-label .required {
            color: red;
            margin-left: 4px;
        }


        .form-group .form-input {
            width: 70%;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: row;
            flex: 1 1 0;
            /* line-height: 32px; */

        }

        .form-group .form-input .studentImg {
            width: 64px;
        }



        .form-group .form-input input[type="text"] {
            width: 100%;
            font-size: 20px;
            padding: 8px 4px;
            border: 2px solid #4d7aa2;
            border-radius: 2px;
        }

        .form-group .form-input p {
            text-align: left;
        }

        #datepicker {
            width: 180px;
            margin: 0 20px 20px 20px;
        }

        #datepicker>span:hover {
            cursor: pointer;
        }

        .form-group .form-input input[type="radio"] {
            /* width: 30%; */
            /* font-size: 12px; */

            border: 1px solid #4d7aa2;
            background-color: #5b9bd5;
            width: 16px;
        }

        .form-group .form-input>p {
            margin: 0;
            font-weight: 600;
            font-size: 20px;
        }

        .form-group .form-input input[type="radio"]:hover {
            /* border: 1px solid #4d7aa2; */
            cursor: pointer;
            background-color: #5b9bd5;
        }

        .form-group .form-input .radio-label {
            margin-left: 8px;
            margin-bottom: 0;
            font-weight: 500;
        }

        .form-group .form-input #falcuties {
            width: 100%;
            font-size: 20px;
            padding: 10px 4px;
            border: 2px solid #4d7aa2;
            border-radius: 2px;

        }

        select {
            background: url("data:image/svg+xml,<svg height='20px' width='20px' viewBox='0 0 16 16' fill='%23000000' xmlns='http://www.w3.org/2000/svg'><path d='M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z'/></svg>") no-repeat;
            background-position: calc(100% - 0.75rem) center !important;
            -moz-appearance: none !important;
            -webkit-appearance: none !important;
            appearance: none !important;
            padding-right: 2rem !important;
        }

        .form-group .form-input .radio-group {
            display: flex;
            flex-direction: row;
            margin-right: 24px;
        }

        .form-group .form-input .radio-group label {
            font-size: 20px;
        }

        .formDisplay .submit-btn {
            text-align: center;
            width: 120px;
            color: #fff;
            background-color: #70ad47;
            font-size: 14px;
            padding: 12px;
            border: 2px solid #4d7aa2;
            border-radius: 6px;
        }
    </style>
</head>

<body>
    <?php
    // session_start();
    // print($_SESSION);
    ?>
    <div id="main">
        <div class="wrapper">
            <div class="showError">
            </div>
            <div class="formDisplay">
                <div class="form-group">
                    <label class="form-label">Họ và tên <span class="required">*</span></label>
                    <div class="form-input">
                        <?php echo '<p id="name">' . $_SESSION["name"] . '</p>' ?>
                    </div>

                </div>
                <div class="form-group">
                    <label class="form-label">Giới tính <span class="required">*</span></label>
                    <div class="form-input">
                        <?php
                        $gender = "";
                        if ($_SESSION["gender"] == '0') {
                            $gender = "Nam";
                        } else {
                            $gender = "Nữ";
                        }

                        echo '<p id="gender">' . $gender . '</p>' ?>
                    </div>


                </div>
                <div class="form-group">
                    <label class="form-label">Phân khoa <span class="required">*</span></label>
                    <div class="form-input">
                        <?php
                        $falcuty = "";
                        if ($_SESSION["falcuty"] == 'MAT') {
                            $falcuty = "Khoa học máy tính";
                        } else if ($_SESSION["falcuty"] == 'KHD') {
                            $falcuty = "Khoa học dữ liệu";
                        }
                        echo '<p id="falcuty">' . $falcuty . '</p>' ?>
                    </div>


                </div>
                <div class="form-group">
                    <label class="form-label">Ngày sinh <span class="required">*</span></label>
                    <div class="form-input">
                        <?php echo '<p id="dob">' . $_SESSION["dob"] . '</p>' ?>

                    </div>

                </div>
                <div class="form-group">
                    <label class="form-label">Địa chỉ</label>
                    <div class="form-input">
                        <?php
                        echo '<p id="address">' . $_SESSION["address"] . '</p>'
                        ?>
                    </div>

                </div>
                <div class="form-group">
                    <label class="form-label">Hình ảnh</label>
                    <div class="form-input">
                        <?php
                        if (!empty($_SESSION["image"])) {
                            echo '<img class="studentImg" src="' . $_SESSION["image"] . '" alt="Student avatar">';
                        } ?>
                    </div>

                </div>
                <input type="submit" name="submit" class="submit-btn" value="Xác nhận">

            </div>
        </div>
    </div>
    <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
    <!-- Bootstrap -->
    <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- Bootstrap DatePicker -->
    <script type="text/javascript">
        $(function() {
            $('#txtDate').datepicker({
                format: "dd/mm/yyyy"
            });
        });
    </script> -->

</body>

</html>